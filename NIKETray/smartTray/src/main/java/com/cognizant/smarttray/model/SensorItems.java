package com.cognizant.smarttray.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 452781 on 10/4/2016.
 */
public class SensorItems implements Serializable {


    @SerializedName("SensorName")
    @Expose
    private String sensorName;
    @SerializedName("Height")
    @Expose
    private Integer height;
    @SerializedName("TraySensorId")
    @Expose
    private String traySensorId;
    @SerializedName("Weight")
    @Expose
    private Integer weight;
    @SerializedName("ItemBarcode")
    @Expose
    private String itemBarcode;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("SensorDeviceType")
    @Expose
    private String sensorDeviceType;

    /**
     * @return The sensorName
     */
    public String getSensorName() {
        return sensorName;
    }

    /**
     * @param sensorName The SensorName
     */
    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    /**
     * @return The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height The Height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return The traySensorId
     */
    public String getTraySensorId() {
        return traySensorId;
    }

    /**
     * @param traySensorId The TraySensorId
     */
    public void setTraySensorId(String traySensorId) {
        this.traySensorId = traySensorId;
    }

    /**
     * @return The weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight The Weight
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return The itemBarcode
     */
    public String getItemBarcode() {
        return itemBarcode;
    }

    /**
     * @param itemBarcode The ItemBarcode
     */
    public void setItemBarcode(String itemBarcode) {
        this.itemBarcode = itemBarcode;
    }

    /**
     * @return The itemId
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * @param itemId The ItemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * @return The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName The ProductName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return The sensorDeviceType
     */
    public String getSensorDeviceType() {
        return sensorDeviceType;
    }

    /**
     * @param sensorDeviceType The SensorDeviceType
     */
    public void setSensorDeviceType(String sensorDeviceType) {
        this.sensorDeviceType = sensorDeviceType;
    }


}
