package com.cognizant.smarttray.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.net.Uri;

import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.adapter.TrayAdapter;
import com.cognizant.smarttray.adapter.TrayDetailsAdapter;

import com.cognizant.smarttray.model.Response;
import com.cognizant.smarttray.model.SingleTraySlot;
import com.cognizant.smarttray.model.TrayDetailsData;
import com.cognizant.smarttray.network.GsonRequest;
import com.cognizant.smarttray.network.VolleyHelper;
import com.cognizant.smarttray.traycustomization.TrayConfiguration;
import com.cognizant.smarttray.network.Constants;
import com.cognizant.smarttray.utils.AccountState;
import com.cognizant.smarttray.utils.TrayDataPopulate;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


import static com.cognizant.smarttray.utils.TrayDataPopulate.adapterObject;
import static com.cognizant.smarttray.utils.TrayDataPopulate.setData;


public class SingleTrayActivity extends AppCompatActivity {

    private static final String LOG = SingleTrayActivity.class.getName();


    int PLACE_PICKER_REQUEST = 1;

    TextView ircount, irtotalcount, w_count, w_totalcount;
    ImageView call, email, map, traymetrics;
    int position;


    RecyclerView trayDetailsRecyclerView;
    //    private RecyclerView.Adapter trayDetailsAdapter;
//    private TrayAdapter trayDetailsAdapter;

    TrayDetailsAdapter trayDetailsAdapter;


    String connectionToast = "Could not refresh details. Check internet connection";

    Response[] responseObject;

    int c = 1;


    MyTimerTask yourTask = new MyTimerTask();
    Timer timer = new Timer();

    public static final int MY_PERMISSIONS_REQUEST = 123;

    List<TrayDetailsData> detailsdata;
    Response tray = null;

    RequestQueue queue;

    public static List<TrayDetailsData> trayDetailsList = new ArrayList<TrayDetailsData>();
    SingleTraySlot singleTraySlot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        tray = (Response) getIntent().getSerializableExtra("data");
        position = getIntent().getIntExtra("position", 0);

        Log.e(LOG, "*#*#*#*" + String.valueOf(position));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.retail_home);

        call = (ImageView) findViewById(R.id.callinfo);
        email = (ImageView) findViewById(R.id.emailinfo);
        map = (ImageView) findViewById(R.id.mapinfo);
        traymetrics = (ImageView) findViewById(R.id.traymetrics);


        ircount = (TextView) findViewById(R.id.storecount);
        irtotalcount = (TextView) findViewById(R.id.storetotalcount);
        w_count = (TextView) findViewById(R.id.warehousecount_number);
        w_totalcount = (TextView) findViewById(R.id.warehousetotalcount);

        trayDetailsRecyclerView = (RecyclerView) findViewById(R.id.tray_details_view);
        trayDetailsRecyclerView.setHasFixedSize(true);

        setData(tray, null);
        trayDetailsList = adapterObject();
        displayData();


        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trayDetailsRecyclerView.setLayoutManager(layoutManager);


        queue = Volley.newRequestQueue(this);
        if (AccountState.getOfflineMode()) {
            getTraySlotDataOFFLINE();
        } else {
            getData();
            getTraySlotData();
            timer.scheduleAtFixedRate(yourTask, 2000, 2000);  // 5 sec interval
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);
        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callUser("859824743788");
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto: abc@xyz.com"));
                startActivity(Intent.createChooser(emailIntent, "Request Order"));
            }
        });

        traymetrics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openApp(SingleTrayActivity.this, "com.xxmassdeveloper.mpchartexample");
            }
        });


    }

    class MyTimerTask extends TimerTask {
        public void run() {
            //         do whatever you want in here
            getData();


        }
    }


    @Override
    public void onResume() {
        super.onResume();

        //getData();

    }

    private void callUser(String telNumber) {
        if (telNumber.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + telNumber));
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                ChangeSettings();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        this.finish();
    }

    private void ChangeSettings() {
        Intent startIntent = new Intent(this, TrayConfiguration.class);
        startIntent.putExtra("TrayId", tray.getDeviceAgentId());

        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {
        if (timer != null) {
            timer.cancel();
        }
        super.onDestroy();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {

        final String[] requestPer = new String[]{Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_NETWORK_STATE};

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(SingleTrayActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SingleTrayActivity.this, Manifest.permission.INTERNET)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(SingleTrayActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission");
                    alertBuilder.setMessage("Permission");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(SingleTrayActivity.this, requestPer, MY_PERMISSIONS_REQUEST);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(SingleTrayActivity.this, requestPer, MY_PERMISSIONS_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    //code for deny
                }
                break;
        }
    }


    public void displayData() {

//        Log.e("TAG", "before setting adapter" + trayDetailsList.get(1).inStoreCount);

        trayDetailsAdapter = new TrayDetailsAdapter(getApplicationContext(), trayDetailsList);
//        trayDetailsAdapter = new TrayAdapter(getApplicationContext(), trayDetailsList);

        trayDetailsRecyclerView.setAdapter(trayDetailsAdapter);
//        trayDetailsRecyclerView.invalidate();
    }

    public void getData() {
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
//        params.put("TrayId", "CA3B37E3-E96E-467E-97F6-E5291A66346D");

        Log.i(LOG, params.toString());

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("TrayId", tray.getDeviceAgentId());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //API CALL GETTING MADE FOR Dynamic Version

        GsonRequest<Response[]> getResponse =
                new GsonRequest<Response[]>(Request.Method.POST, Constants.REQUEST_SINGLE_TRAY_DATA_URL, Response[].class, params, jsonObject,

                        new com.android.volley.Response.Listener<Response[]>() {
                            @Override
                            public void onResponse(Response[] response) {

                                tray = response[0];
                                Log.e("TAG", "_____----- Tray Name " + tray.getTrayName());
                                Log.e("TAG", "_____----- Weight Sensor " + tray.getWTSensor001());
//                                Log.e("TAG", "_____----- UV Sensor " + tray.getUVSensor001());
//                                Log.e("TAG", "_____----- UV Sensor " + tray.getUVSensor002());
                                Log.e("TAG", "_____----- IR Sensor " + tray.getIRSensor001());
                                Log.e("TAG", "_____----- IR Sensor " + tray.getIRSensor002());


                                setData(response[0], singleTraySlot);
//                                displayData(adapterObject());
//
                                trayDetailsList = adapterObject();
                                trayDetailsAdapter.notifyDataSetChanged();
//                                trayDetailsRecyclerView.invalidate();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(SingleTrayActivity.this, connectionToast, Toast.LENGTH_SHORT).show();
                            connectionToast = "";

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getResponse.setRetryPolicy(policy);
//        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getResponse);

        queue.add(getResponse);
    }


    public void getTraySlotData() {
        Log.e("TAG", "_____getTraySlotData CALLED");

        StringRequest strRequest = new StringRequest(Request.Method.POST, Constants.REQUEST_TRAY_SINGLE_CONFIGURATION_URL,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TAG", "_____response-----" + response);
                        try {
                            JSONObject responsejson = new JSONObject(response);

                            Gson gson = new Gson();
                            singleTraySlot = gson.fromJson(response, SingleTraySlot.class);
//                            Log.e("TAG", "_____-----" + singleTraySlot.getSingleTraySlots());

                            setData(tray, singleTraySlot);
                            trayDetailsList = adapterObject();
//                            displayData();
                            System.out.println("@@## singleTraySlot " + singleTraySlot.getValue().get(0).getProductName());

                            trayDetailsAdapter.notifyDataSetChanged();
//                            trayDetailsRecyclerView.invalidate();


                        } catch (IndexOutOfBoundsException | JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("TAG", "_____VolleyError");
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Log.e("TAG", "getParams");
                Map<String, String> params = new HashMap<String, String>();
//                params.put("TrayId", "CA3B37E3-E96E-467E-97F6-E5291A66346D");
                params.put("TrayId", tray.getDeviceAgentId());
                return params;
            }
        };

        queue.add(strRequest);


    }


    public boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            Toast.makeText(SingleTrayActivity.this, "Contact Service Provider", Toast.LENGTH_LONG).show();
            return false;
            //throw new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }


    public void showLocationPickerDialog(View v) {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
            Toast.makeText(this, "Server error. Please try after sometime.", Toast.LENGTH_LONG).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            Toast.makeText(this, "Unexpected error. Please try after sometime.", Toast.LENGTH_LONG).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    /*
    OFFLINE MODE
     */

    public void getTraySlotDataOFFLINE() {


        try {


            Gson gson = new Gson();
            singleTraySlot = gson.fromJson(loadsingletrayconfigResponse(), SingleTraySlot.class);
//                            Log.e("TAG", "_____-----" + singleTraySlot.getSingleTraySlots());

            setData(tray, singleTraySlot);
            trayDetailsList = adapterObject();
//                            displayData();
            System.out.println("@@## singleTraySlot " + singleTraySlot.getValue().get(0).getProductName());

            trayDetailsAdapter.notifyDataSetChanged();
//                            trayDetailsRecyclerView.invalidate();


        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


    }


    public String loadsingletrayconfigResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("trayconfig.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}



