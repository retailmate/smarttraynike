package com.cognizant.smarttray.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.smarttray.orders.OrderActivity;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.activity.TrayDetailActivity;
import com.cognizant.smarttray.model.TrayDetailsData;

import java.util.Collections;
import java.util.List;

/**
 * Created by 452781 on 8/17/2016.
 */
public class TrayDetailsAdapter extends RecyclerView.Adapter<TrayDetailsAdapter.myViewHolder> {

    private final LayoutInflater inflater;

    private Context mContext;

    List<TrayDetailsData> dispdata = Collections.emptyList();

    TrayDetailsData trayDetailsData;

    int viewPosition;

    public TrayDetailsAdapter(Context context, List<TrayDetailsData> dispdata) {
        inflater = LayoutInflater.from(context);
        this.dispdata = dispdata;

        System.out.println("@@## TrayDetailsAdapter" + dispdata.size());

        this.mContext = context;

    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_tray_layout, parent, false);
        myViewHolder holder = new myViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {

        viewPosition = position;

        System.out.println("@@## position" + position);

        trayDetailsData = dispdata.get(position);
        holder.titleView.setText(trayDetailsData.title);
        if (trayDetailsData.inStoreCount.equals("loading..")) {
            holder.inStoreCountView.setText(Html.fromHtml("<i>" + "<small>" + trayDetailsData.inStoreCount + "</small>" + "</i>"));
        } else {
            holder.inStoreCountView.setText(trayDetailsData.inStoreCount);
        }
        holder.inStoreTotalCountView.setText(trayDetailsData.inStoreTotalCount);
        holder.warehouseCountView.setText(trayDetailsData.warehouseCount);
        holder.warehouseTotalCountView.setText(trayDetailsData.warehouseTotalCount);


        if (trayDetailsData.inStoreColour.equals("red")) {
            holder.inStoreCountView.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            holder.colourBarView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red));
            //return;
        }
    }

    @Override
    public int getItemCount() {
//        System.out.println("@@## dispdata.size()" + dispdata.size());
        return dispdata.size();


    }

    class myViewHolder extends RecyclerView.ViewHolder {
        TextView inStoreCountView, inStoreTotalCountView, warehouseCountView, warehouseTotalCountView, titleView, orderNowView;
        View colourBarView;
        LinearLayout trayDetailsView;


        public myViewHolder(View itemView) {
            super(itemView);

            colourBarView = itemView.findViewById(R.id.order_status);
            titleView = (TextView) itemView.findViewById(R.id.content_name);
            inStoreCountView = (TextView) itemView.findViewById(R.id.storecount);
            inStoreTotalCountView = (TextView) itemView.findViewById(R.id.storetotalcount);
            warehouseCountView = (TextView) itemView.findViewById(R.id.warehousecount_number);
            warehouseTotalCountView = (TextView) itemView.findViewById(R.id.warehousetotalcount);
            orderNowView = (TextView) itemView.findViewById(R.id.orderNow);
            trayDetailsView = (LinearLayout) itemView.findViewById(R.id.tray_recyc_layout);


            trayDetailsView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(v.getContext(),dispdata.get(getAdapterPosition()).title ,Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(v.getContext(), TrayDetailActivity.class);
                    intent.putExtra("traydata", dispdata.get(getAdapterPosition()));
                    intent.putExtra("colour", dispdata.get(getAdapterPosition()).inStoreColour);
                    intent.putExtra("limit", dispdata.get(getAdapterPosition()).inStoreTotalCount.split(" ", 2)[0].replaceAll("g", ""));

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);

                }
            });


            orderNowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(v.getContext(),"Order",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(v.getContext(), OrderActivity.class);
                    intent.putExtra("title", dispdata.get(getAdapterPosition()).title);
                    intent.putExtra("limit", dispdata.get(getAdapterPosition()).inStoreTotalCount.split(" ", 2)[0].replaceAll("g", ""));
                    intent.putExtra("sensorid", dispdata.get(getAdapterPosition()).poID);
                    System.out.println("@@## POID " + dispdata.get(getAdapterPosition()).poID);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });


        }
    }
}