package com.cognizant.smarttray.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.smarttray.orders.OrderActivity;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.uservalidation.LoginActivity;
import com.cognizant.smarttray.model.TrayDetailsData;
import com.cognizant.smarttray.unused.SettingsIRAlert;
import com.cognizant.smarttray.utils.SettingsWAlert;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;


public class TrayDetailActivity extends AppCompatActivity {

    private static final String LOG = LoginActivity.class.getName();

    private static final String TAG = TrayDetailActivity.class.getName();
    String hint;
    View status;
    TextView count_view, totalcount_view, w_count, w_totalcount, manufacturer_view, shelf_view, tray_view;
    Button manualRefill, orderNow;

    TrayDetailsData trayDetailsData;
    ProgressDialog progressDialog;

    ImageView call, email, map;
    int limit;
    int PLACE_PICKER_REQUEST = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tray_information_detail);

        trayDetailsData = (TrayDetailsData) getIntent().getSerializableExtra("traydata");
        limit = Integer.parseInt(getIntent().getStringExtra("limit"));

        Log.e("TAG", "_______" + limit + "- Value received at tray detail activity");
        String colour = getIntent().getStringExtra("colour");

        hint = trayDetailsData.warehouseTotalCount;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        call = (ImageView) findViewById(R.id.callinfo);
        email = (ImageView) findViewById(R.id.emailinfo);
        map = (ImageView) findViewById(R.id.mapinfo);

        status = findViewById(R.id.status_det);

        manualRefill = (Button) findViewById(R.id.manualRefil);
        orderNow = (Button) findViewById(R.id.order);

        count_view = (TextView) findViewById(R.id.inStoreDetail);
        totalcount_view = (TextView) findViewById(R.id.inStoreTotalDetail);

        w_count = (TextView) findViewById(R.id.warehouseCountDetail);
        w_totalcount = (TextView) findViewById(R.id.warehouseCountTotalDetail);

        manufacturer_view = (TextView) findViewById(R.id.manufacturer);
        shelf_view = (TextView) findViewById(R.id.shelf);
        tray_view = (TextView) findViewById(R.id.trayinfo);


        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
//            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);

        }

        count_view.setText(trayDetailsData.inStoreCount);
        totalcount_view.setText(trayDetailsData.inStoreTotalCount);

        w_count.setText(trayDetailsData.warehouseCount);
        w_totalcount.setText(trayDetailsData.warehouseTotalCount);


        if (colour.equals("red")) {
            count_view.setTextColor(ContextCompat.getColor(TrayDetailActivity.this, R.color.red));
            status.setBackgroundColor(ContextCompat.getColor(TrayDetailActivity.this, R.color.red));
        }

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callUser("859824743788");
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto: abc@xyz.com"));
                startActivity(Intent.createChooser(emailIntent, "Request Order"));
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void callUser(String telNumber) {
        if (telNumber.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + telNumber));
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // inflater.inflate(R.menu.actionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                ChangeSettings();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }


    private void ChangeSettings() {
        Intent startIntent = null;
        if (hint.contains("unit")) {
            startIntent = new Intent(this, SettingsWAlert.class);
//            startIntent.putExtra("limit", limit);
        } else {
            startIntent = new Intent(this, SettingsIRAlert.class);
            startIntent.putExtra("limit", limit);
        }
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startIntent);
    }


    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public void showLocationPickerDialog(View v) {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showOrderPage(View v) {

        Intent intent = new Intent(TrayDetailActivity.this, OrderActivity.class);
        intent.putExtra("title", trayDetailsData.title);
        intent.putExtra("sensorid", trayDetailsData.poID);
        System.out.println("@@## POID " + trayDetailsData.poID);

        intent.putExtra("limit", String.valueOf(limit));
        startActivity(intent);

    }

    public void showManualRefill(View v) {
        progressDialog = new ProgressDialog(TrayDetailActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progressDialog.cancel();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(TrayDetailActivity.this);
                alertDialog.setTitle("Success")
                        .setMessage("Manual refill successful.")
                        .setCancelable(false)
                        .setIconAttribute(android.R.attr.alertDialogIcon)
                        .setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .show();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 1500);


    }
}