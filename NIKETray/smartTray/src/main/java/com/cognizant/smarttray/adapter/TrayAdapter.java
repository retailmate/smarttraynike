package com.cognizant.smarttray.adapter;

/**
 * Created by 452781 on 9/30/2016.
 */


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.model.TrayDetailsData;

import java.util.List;

public class TrayAdapter extends RecyclerView.Adapter<TrayAdapter.MyViewHolder> {

    private List<TrayDetailsData> traysList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.content_name);

        }
    }


    public TrayAdapter(Context context, List<TrayDetailsData> traysList) {
        this.traysList = traysList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tray_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TrayDetailsData tray = traysList.get(position);
        holder.title.setText(tray.title);

    }

    @Override
    public int getItemCount() {
        return traysList.size();

    }
}
