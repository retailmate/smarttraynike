

/**
 * Created by 452781 on 10/4/2016.
 */


package com.cognizant.smarttray.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WTSensorConfiguration {

    @SerializedName("SensorId")
    @Expose
    private String sensorId;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("MaxWt")
    @Expose
    private Double maxWt;
    @SerializedName("WtThreshold")
    @Expose
    private Double wtThreshold;

    /**
     * @return The sensorId
     */
    public String getSensorId() {
        return sensorId;
    }

    /**
     * @param sensorId The SensorId
     */
    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    /**
     * @return The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId The ProductId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return The maxWt
     */
    public Double getMaxWt() {
        return maxWt;
    }

    /**
     * @param maxWt The MaxWt
     */
    public void setMaxWt(Double maxWt) {
        this.maxWt = maxWt;
    }

    /**
     * @return The wtThreshold
     */
    public Double getWtThreshold() {
        return wtThreshold;
    }

    /**
     * @param wtThreshold The WtThreshold
     */
    public void setWtThreshold(Double wtThreshold) {
        this.wtThreshold = wtThreshold;
    }

}