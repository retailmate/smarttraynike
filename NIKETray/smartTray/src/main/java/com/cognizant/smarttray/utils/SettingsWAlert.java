package com.cognizant.smarttray.utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;

import com.cognizant.smarttray.R;

/**
 * Created by 452781 on 8/4/2016.
 */
public class SettingsWAlert extends Activity {

    String message = "", notificationType = "";
    Long jobassignid;
    Boolean ishistory;
    Dialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getIntent() != null) {
            message = getIntent().getStringExtra("message");
            jobassignid = getIntent().getLongExtra("jobassignid", 0);
            notificationType = getIntent().getStringExtra("notitype");
        }


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        d = new Dialog(this, R.style.NewDialog);
        LayoutInflater infalInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        View mView = infalInflater.inflate(R.layout.setting_threshold_w, null);
        Button ok = (Button) mView.findViewById(R.id.okButon);
        Button cancel = (Button) mView.findViewById(R.id.cancelButton);
        NumberPicker weightcountpicker = (NumberPicker) mView.findViewById(R.id.weightpicker);

        weightcountpicker.setMinValue(0);
        weightcountpicker.setMaxValue(100);
        weightcountpicker.setFormatter(formatter);
        weightcountpicker.setWrapSelectorWheel(true);

        //Set a value change listener for NumberPicker


        //Set a value change listener for NumberPicker
        weightcountpicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //Display the newly selected number from picker
                // tv.setText("Selected Number : " + newVal);

            }
        });


        d.setContentView(mView);

        Window window = d.getWindow();
        window.setLayout(screenWidth, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();

                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();

                finish();
            }
        });
        d.show();
    }

    NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
        @Override
        public String format(int value) {
            int temp = value * 10;
            return "" + temp;
        }
    };

    @Override
    public void onBackPressed() {
        d.dismiss();
        this.finish();
    }
}

