package com.cognizant.smarttray.orders;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.uservalidation.LoginActivity;
import com.cognizant.smarttray.network.Constants;
import com.cognizant.smarttray.utils.AccountState;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class OrderActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    private static final String LOG = LoginActivity.class.getName();

    private static final String TAG = OrderActivity.class.getName();

    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

    int PLACE_PICKER_REQUEST = 1;
    String poid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_now);
        int limit = Integer.parseInt(getIntent().getStringExtra("limit"));

        String title = getIntent().getStringExtra("title");

        poid = getIntent().getStringExtra("sensorid");


        System.out.println("@@## POID " + poid);


        TextView titleView = (TextView) findViewById(R.id.order_title);
        titleView.setText(title);


        NumberPicker ircountpicker = (NumberPicker) findViewById(R.id.orderpicker);
        ircountpicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        ircountpicker.setWrapSelectorWheel(true);
        ircountpicker.setMinValue(1);
        ircountpicker.setMaxValue(limit);
        ircountpicker.setWrapSelectorWheel(true);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);
        }

    }

    public void payment(View view) {

        if (AccountState.getOfflineMode()) {
            Toast.makeText(OrderActivity.this, "Order Placed Successfully.", Toast.LENGTH_LONG).show();
        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CREATE_PO_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(OrderActivity.this, "Order Placed Successfully." + response, Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(OrderActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("_parmStoreID", "Etihad");
                    params.put("_parmTrayID", poid);
                    params.put("_parmQty", "20");
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }


    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
        }
    }

    public void showLocationPickerDialog(View v) {

        new mapTask().execute();


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }


    class mapTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OrderActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                startActivityForResult(builder.build(OrderActivity.this), PLACE_PICKER_REQUEST);


            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
                Toast.makeText(OrderActivity.this, "Server error. Please try after sometime.", Toast.LENGTH_LONG).show();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
                Toast.makeText(OrderActivity.this, "Unexpected error. Please try after sometime.", Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }
    }


}






