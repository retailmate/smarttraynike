package com.cognizant.smarttray.barcode;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.widget.Toast;

import com.cognizant.smarttray.traycustomization.TrayConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ScanActivity extends Activity implements
        PreviewCallback, ZBarConstants {

    private static final String TAG = "ZBarScannerActivity";
    private static final String CustomTag = "CustomTag";
    private static final int REQUEST_CAMERA = 111;
    private CameraPreview mPreview;
    private Camera mCamera;
    private ImageScanner mScanner;
    private Handler mAutoFocusHandler;
    private boolean mPreviewing = true;
    Map<String, String> QRMap = new HashMap<String, String>();

    static {
        System.loadLibrary("iconv");
    }


    String listParentName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isCameraAvailable()) {
            // Cancel request if there is no rear-facing camera.
            cancelRequest();
            return;
        }


        listParentName = getIntent().getStringExtra("listParentName");

        QRMap = (Map<String, String>) getIntent().getSerializableExtra("QRcodeNameMAP");


        //loading json from assets folder
        String jsonString = loadJSONFromAsset();


//        JSONObject obj = null;
//        try {
//            //Reading Location in form of JSON String returned by loadJSONFromAsset()
//            obj = new JSONObject(jsonString);
//            // Creating JSONArray from JSONObject
//            JSONArray jsonArray = obj.getJSONArray("products");
//
//            for (int i = 0; i < jsonArray.length(); i++) {
//
//                // Creating JSONObject from JSONArray
//                JSONObject jsonObj = jsonArray.getJSONObject(i);
//
//                // Getting data from individual JSONObject
//
//                String id = jsonObj.getString("id");
//                String name = jsonObj.getString("name");
//                QRMap.put(id, name);
//                Log.d(CustomTag, "id = " + id + " name = " + name);
//
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        // Hide the window title.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Display display = getWindowManager().getDefaultDisplay();
        int screen_width = display.getWidth();
        int screen_height = display.getHeight();
        //getWindow().setLayout(screen_width-40,screen_height-80);
        //  getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mAutoFocusHandler = new Handler();

        // Create and configure the ImageScanner;
        setupScanner();

        // Create a RelativeLayout container that will hold a SurfaceView,
        // and set it as the content of our activity.
        mPreview = new CameraPreview(this, this, autoFocusCB);
        setContentView(mPreview);
    }

    public void setupScanner() {
        mScanner = new ImageScanner();
        mScanner.setConfig(0, Config.X_DENSITY, 3);
        mScanner.setConfig(0, Config.Y_DENSITY, 3);

        int[] symbols = getIntent().getIntArrayExtra(SCAN_MODES);
        if (symbols != null) {
            mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
            for (int symbol : symbols) {
                mScanner.setConfig(symbol, Config.ENABLE, 1);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23) {
            //request permissions
            int hasCameraPermission = ContextCompat.checkSelfPermission(ScanActivity.this,
                    Manifest.permission.CAMERA);
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(ScanActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA);

            } else {
                openCamera();
            }

        } else {
            openCamera();
        }

    }

    public void openCamera() {
        mCamera = Camera.open();

        if (mCamera == null) {
            // Cancel request if mCamera is null.
            cancelRequest();
            return;
        }
        mPreview.setCamera(mCamera);
        mPreview.showSurfaceView();

        mPreviewing = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                // Open the default i.e. the first rear facing camera.
                openCamera();

            } else {
                // Permission Denied
                Toast.makeText(ScanActivity.this, "Camera Not available", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.cancelAutoFocus();
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();


            mPreview.hideSurfaceView();

            mPreviewing = false;
            mCamera = null;
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void cancelRequest() {
        Intent dataIntent = new Intent();
        dataIntent.putExtra(ERROR_INFO, "Camera unavailable");
        setResult(Activity.RESULT_CANCELED, dataIntent);
        finish();
    }

    public void onPreviewFrame(byte[] data, Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPreviewSize();

        Image barcode = new Image(size.width, size.height, "Y800");
        barcode.setData(data);

        int result = mScanner.scanImage(barcode);

        if (result != 0) {
            mCamera.cancelAutoFocus();
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mPreviewing = false;
            SymbolSet syms = mScanner.getResults();
            for (Symbol sym : syms) {
                String symData = sym.getData();

                if (!TextUtils.isEmpty(symData)) {
                    Intent dataIntent = new Intent(ScanActivity.this, InitialActivity.class);
                    if (QRMap.containsKey(symData)) {


//                        int itemID = Integer.parseInt(symData);
                        String itemID = symData;

                        TrayConfiguration.listDataChild.get(listParentName).get(0).setProductName(QRMap.get(symData));
                        TrayConfiguration.listDataChild.get(listParentName).get(0).setItemId(QRMap.get(QRMap.get(symData)));


                        TrayConfiguration.notifychange();

                        finish();
                        break;

                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid Product, please try again", Toast.LENGTH_SHORT).show();

                        finish();
                        break;

                    }


                }
            }
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (mCamera != null && mPreviewing) {
                mCamera.autoFocus(autoFocusCB);
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            mAutoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("products.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


}
